import Vue from "vue";
import VueRouter from "vue-router";
import Events from "./components/events/Events";
import Statistic from "./components/statistic/Statistic";

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    {
      component: Events,
      path: "/events"
    },
    {
      component: Statistic,
      path: "/"
    }
  ],
  mode: "history"
});
