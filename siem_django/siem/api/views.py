from django.shortcuts import render
from .models import Event
from .serializator import EvetSerializer
from .models import Event
from rest_framework import viewsets

# Create your views here.


class EventViewSet(viewsets.ModelViewSet):
    serializer_class = EvetSerializer

    def get_queryset(self):
        queryset = Event.objects.all()
        id_event = self.request.query_params.get('id', None)
        event = self.request.query_params.get('event', None)
        if id_event is not None:
            queryset = queryset.filter(id=id_event)
        elif event is not None:
            queryset = queryset.filter(event=event)
        return queryset
