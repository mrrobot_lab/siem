from rest_framework import serializers
from .models import Event


class EvetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = '__all__'
