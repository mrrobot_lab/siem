from django.db import models
from django.db import models
# Create your models here.


class Event(models.Model):
    time_stamp = models.TimeField(auto_now=True)
    event = models.CharField(max_length=300)
    host = models.CharField(max_length=30)
    host_name = models.CharField(max_length=30)
    user_name = models.CharField(max_length=30)
    src_ip = models.CharField(max_length=30)
    dst_ip = models.CharField(max_length=30)
    dst_port = models.CharField(max_length=30)
    program = models.CharField(max_length=30)
    
    class Meta:
        ordering = ['time_stamp']
    